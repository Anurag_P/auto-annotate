## Traffic Video Annotation
### Opencv based tool for tagging new vehicles/traffic classes
(*works only on videos and only for moving objects*)


Using opencv features like **background subtraction** and **motion history image** I have tried creating a bounding box around moving objects.

It has trackbars which can help choose bounding box sizes and aspect ratio. This selection could help in removing false positives.

### Usage

`python motempl.py`

### Future work
- **Extract images** from video frame using bbox information
- **Feature embedding based similarity** is done for semi-supervised training of new groups of objects.
- Opencv techniques like **color histogram** can be applied to find similar objects


Although higher fps (6-10) are needed to get motion history, extraction should be done at a lower fps to avoid multiple instances of the same traffic object.
requirements file has extra libraries. Needs cleaup